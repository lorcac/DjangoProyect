# DjangoProyect
1) se creo el servidor con comando: django-admin startproject trabajodjando

2) se crea una app con el comando: python manage.py startapp postulaciones

3) se agrega el urls.py con la siguiente informacion:

			from django.urls import path

		from . import views

		urlpatterns = [
			path('', views.index, name='index'),
		]

4) se crean modelos en postulaciones (en postulaciones/models.py):

		from django.db import models


		class Question(models.Model):
			question_text = models.CharField(max_length=200)
			pub_date = models.DateTimeField('date published')


		class Choice(models.Model):
			question = models.ForeignKey(Question, on_delete=models.CASCADE)
			choice_text = models.CharField(max_length=200)
			votes = models.IntegerField(default=0)
			
5) se edita settings.py para agregar los modelos de postulaciones en INSTALLED_APPS:

		INSTALLED_APPS = [
		""	'postulaciones.apps.PostulacionesConfig',		""
			'django.contrib.admin',
			'django.contrib.auth',
			'django.contrib.contenttypes',
			'django.contrib.sessions',
			'django.contrib.messages',
			'django.contrib.staticfiles',
		]
6) se generan las migraciones con comando: py manage.py makemigrations

7) 	luego se migran las migraciones creadas a la base de datos con comando: py manage.py sqlmigrate
	esto generara una consulta de base de datos para crear las tablas correspondientes a los modelos creados.

8) se finaliza las migraciones con comando : py manage.py migrate

9) se crea el superusuario con: py manage.py createsuperuser

10) se crean las vistas de postulaciones:

		from django.http import HttpResponse

		from .models import Question


		def index(request):
			latest_question_list = Question.objects.order_by('-pub_date')[:5]
			output = ', '.join([q.question_text for q in latest_question_list])
			return HttpResponse(output)
		    return HttpResponse("You're looking at question %s." % question_id)

		def results(request, question_id):
			response = "You're looking at the results of question %s."
			return HttpResponse(response % question_id)

		def vote(request, question_id):
			return HttpResponse("You're voting on question %s." % question_id)

12) se edita el urls.py con la siguiente informacion:

		from django.urls import path

		from . import views

		urlpatterns = [
			path('', views.index, name='index'),
			path('<int:question_id>/', views.detail, name='detail'),
			path('<int:question_id>/results/', views.results, name='results'),
			path('<int:question_id>/vote/', views.vote, name='vote'),
			
		]

13) se crea una carpeta con direccion "trabajodjando/postulaciones/templates/postulaciones" los templates, en este caso "Index.html"
	
	


	